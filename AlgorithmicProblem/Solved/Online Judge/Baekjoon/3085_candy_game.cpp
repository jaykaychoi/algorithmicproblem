#pragma warning (disable:4996)
#include <cstdio>
#include <cstring>
#include <climits>
#include <algorithm>

using namespace std;

//https://www.acmicpc.net/problem/3085

const int MAX = 50;

int countEatableCandy(int n, char candies[MAX][MAX])
{
	int maxCount = 1;
	for (int x = 0; x < n; x++)
	{
		for (int y = 0; y < n; y++)
		{
			int curCount = 1;
			char startChar = candies[x][y];

			// to right
			for (int x2 = x + 1; x2 < n; x2++)
			{
				if (candies[x2][y] == startChar)
				{
					curCount++;
				}
				else
				{
					break;
				}
			}
			maxCount = max(maxCount, curCount);

			// to down
			curCount = 1;
			for (int y2 = y + 1; y2 < n; y2++)
			{
				if (candies[x][y2] == startChar)
				{
					curCount++;
				}
				else
				{
					break;
				}
			}
			maxCount = max(maxCount, curCount);
		}
	}
	return maxCount;
}

int main()
{
	int n;
	char candies[MAX][MAX];
	scanf("%d", &n);
	for (int x = 0; x < n; x++)
	{
		char row[MAX];
		scanf("%s", row);
		for (int j = 0; j < n; j++)
		{
			candies[x][j] = row[j];
		}
	}

	int maxCandy = 0;
	for (int x = 0; x < n; x++)
	{
		for (int y = 0; y < n; y++)
		{
			char newCandies[MAX][MAX];

			// left
			if (x - 1 > 0)
			{
				memcpy(newCandies, candies, MAX * MAX * sizeof(char));
				newCandies[x - 1][y] = candies[x][y];
				newCandies[x][y] = candies[x - 1][y];
				maxCandy = max(maxCandy, countEatableCandy(n, newCandies));
			}

			// right
			if (x + 1 < n)
			{
				memcpy(newCandies, candies, MAX * MAX * sizeof(char));
				newCandies[x + 1][y] = candies[x][y];
				newCandies[x][y] = candies[x + 1][y];
				maxCandy = max(maxCandy, countEatableCandy(n, newCandies));
			}

			// down
			if (y - 1 > 0)
			{
				memcpy(newCandies, candies, MAX * MAX * sizeof(char));
				newCandies[x][y - 1] = candies[x][y];
				newCandies[x][y] = candies[x][y - 1];
				maxCandy = max(maxCandy, countEatableCandy(n, newCandies));
			}

			// up
			if (y + 1 < n)
			{
				memcpy(newCandies, candies, MAX * MAX * sizeof(char));
				newCandies[x][y + 1] = candies[x][y];
				newCandies[x][y] = candies[x][y + 1];
				maxCandy = max(maxCandy, countEatableCandy(n, newCandies));
			}
		}
	}

	printf("%d\n", maxCandy);
	system("PAUSE");
	return 0;
}